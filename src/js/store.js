// Separating the model to another Class - might be a class handling logic and accessing data elsewhere
export default class PersonModel{
  constructor(fullname){
    this.fullname = fullname;
  }
  // Testing model-side methods
  promote(){
    this.fullname = "The King"
  }
  reset(){
    this.fullname = "";
  }
}