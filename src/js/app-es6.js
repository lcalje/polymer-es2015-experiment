import PersonModel from 'store';

/**
 * Using Polymer 1.0 in ES6 style.
 * Might be a bit off - had to find the way through trial and error.
 * A bit awkward still, unable to use classes and methods the way I wanted to...
 * Maybe ES6 classes will be 1st class citizens in Polymer some day.
 * IMHO, Polymer seems nice: All is pure components.
  * Components are relatively easy to compose.
  * But maybe a bit too little to handle data and the app level things after all? I DON'T like the idea of placing abstract things like data sources (Firebird sample) in the html as elements. Just does not feel right?
 */

class AppController extends Polymer.Class(
  {
    is: "app-view"
  }
){
  ready() {
    console.log("App ready");
  }
}

class PersonController extends Polymer.Class(
    {
        is: "person-view",
        properties: {
          fullname: {
              type: String,
            	value: "Danny Default",
              notify: true
            }
        }
    }
	){
  
	reset(){
    this.model.reset();
    // Won't show in the view before this:
    this.notifyPath('model.fullname', this.model.fullname);
  }
  promote(){
    this.model.promote();
    // Won't show in the view before this:
    this.notifyPath('model.fullname', this.model.fullname);
  }
  created(){
    // Attn: do not use 'this' at this stage yet.
    console.log("Controller Created");
  }
  ready() {
    // Now we can use this.fullname etc.
    console.log(this.fullname); 
    this.model = new PersonModel(this.fullname);
    console.log("Controller ready for:", this.fullname);
  }
  
  attached() {
    console.log("Controller attached", this);
  }
}
// This is needed / compare also with the pure ES6 demo:
document.registerElement(AppController.prototype.is, AppController);
document.registerElement(PersonController.prototype.is, PersonController);

